let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { PagingParams, MultiString } from 'pip-services3-commons-node';

import { DeviceV1 } from '../../src/version1/DeviceV1';
import { IDevicesClientV1 } from '../../src/version1/IDevicesClientV1';

let DEVICE1: DeviceV1 = {
    id: '1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: new Date()
};
let DEVICE2: DeviceV1 = {
    id: '2',
    request_code: '',
    description: 'Device 2',
    last_connection: new Date()
};

export class DevicesClientFixtureV1 {
    private _client: IDevicesClientV1;
    
    constructor(client: IDevicesClientV1) {
        this._client = client;
    }
        
    testCrudOperations(done) {
        let device1, device2: DeviceV1;

        async.series([
        // Create one device
            (callback) => {
                this._client.createDevice(
                    null,
                    DEVICE1,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE1.id);
                        assert.equal(device.request_code, DEVICE1.request_code);
                        assert.equal(device.description, DEVICE1.description);

                        device1 = device;

                        callback();
                    }
                );
            },
        // Create another device
            (callback) => {
                this._client.createDevice(
                    null,
                    DEVICE2,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.id, DEVICE2.id);
                        assert.isNotNull(device.request_code);
                        assert.equal(device.description, DEVICE2.description);

                        device2 = device;

                        callback();
                    }
                );
            },
        // Get all Devices
            (callback) => {
                this._client.getDevices(
                    null,
                    null,
                    new PagingParams(0,5,false),
                    (err, Devices) => {
                        assert.isNull(err);

                        assert.isObject(Devices);
                        assert.isTrue(Devices.data.length >= 2);

                        callback();
                    }
                );
            },
        // Update the device
            (callback) => {
               
                device1.description = 'Updated Name 1';

                this._client.updateDevice(
                    null,
                    device1,
                    (err, device) => {
                        assert.isNull(err);

                        assert.isObject(device);
                        assert.equal(device.description, 'Updated Name 1');
                        assert.equal(device.id, DEVICE1.id);

                        device1 = device;

                        callback();
                    }
                );
            },
            // Generate request code
            (callback) => {
                this._client.generateRequstCode(
                    null,
                    device1.id,
                    (err, code) => {
                        assert.isNull(err);
                        assert.isNotNull(code);
                        callback();
                    }
                );
            },
        // Delete device
            (callback) => {
                this._client.deleteDeviceById(
                    null,
                    device1.id,
                    (err) => {
                        assert.isNull(err);

                        callback();
                    }
                );
            },
        // Try to get delete device
            (callback) => {
                this._client.getDeviceById(
                    null,
                    device1.id,
                    (err, device) => {
                        assert.isNull(err);
                        
                        assert.isNull(device || null);

                        callback();
                    }
                );
            }
        ], done);
    }
}
