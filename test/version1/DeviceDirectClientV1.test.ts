let assert = require('chai').assert;
let async = require('async');

import { Descriptor } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { ConsoleLogger } from 'pip-services3-components-node';

import { DevicesMemoryPersistence } from 'ad-board-services-Devices-node';
import { DevicesController } from 'ad-board-services-Devices-node';
import { DevicesDirectClientV1 } from '../../src/version1/DevicesDirectClientV1';
import { DevicesClientFixtureV1 } from './DevicesClientFixtureV1';

suite('DevicesDirectClientV1', ()=> {
    let client: DevicesDirectClientV1;
    let fixture: DevicesClientFixtureV1;

    suiteSetup((done) => {
        let logger = new ConsoleLogger();
        let persistence = new DevicesMemoryPersistence();
        let controller = new DevicesController();

        let references: References = References.fromTuples(
            new Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger,
            new Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-devices', 'controller', 'default', 'default', '1.0'), controller,
        );
        controller.setReferences(references);

        client = new DevicesDirectClientV1();
        client.setReferences(references);

        fixture = new DevicesClientFixtureV1(client);

        client.open(null, done);
    });
    
    suiteTeardown((done) => {
        client.close(null, done);
    });

    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

});
