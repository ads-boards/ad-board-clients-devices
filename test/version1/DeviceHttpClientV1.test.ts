let assert = require('chai').assert;
let async = require('async');

import { Descriptor } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { References } from 'pip-services3-commons-node';
import { ConsoleLogger } from 'pip-services3-components-node';

import { DevicesMemoryPersistence } from 'ad-board-services-Devices-node';
import { DevicesController } from 'ad-board-services-Devices-node';
import { DevicesHttpServiceV1 } from 'ad-board-services-Devices-node';
import { DevicesHttpClientV1 } from '../../src/version1/DevicesHttpClientV1';
import { DevicesClientFixtureV1 } from './DevicesClientFixtureV1';

var httpConfig = ConfigParams.fromTuples(
    "connection.protocol", "http",
    "connection.host", "localhost",
    "connection.port", 3000
);

suite('DevicesRestClientV1', ()=> {
    let service: DevicesHttpServiceV1;
    let client: DevicesHttpClientV1;
    let fixture: DevicesClientFixtureV1;

    suiteSetup((done) => {
        let logger = new ConsoleLogger();
        let persistence = new DevicesMemoryPersistence();
        let controller = new DevicesController();

        service = new DevicesHttpServiceV1();
        service.configure(httpConfig);

        let references: References = References.fromTuples(
            new Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger,
            new Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '1.0'), persistence,
            new Descriptor('ad-board-devices', 'controller', 'default', 'default', '1.0'), controller,
            new Descriptor('ad-board-devices', 'service', 'http', 'default', '1.0'), service
        );
        controller.setReferences(references);
        service.setReferences(references);

        client = new DevicesHttpClientV1();
        client.setReferences(references);
        client.configure(httpConfig);

        fixture = new DevicesClientFixtureV1(client);

        service.open(null, (err) => {
            client.open(null, done);
        });
    });
    
    suiteTeardown((done) => {
        client.close(null);
        service.close(null, done);
    });

    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

});
