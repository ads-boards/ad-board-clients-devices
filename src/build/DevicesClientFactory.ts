import { Descriptor } from 'pip-services3-commons-node';
import { Factory } from 'pip-services3-components-node';

import { DevicesNullClientV1 } from '../version1/DevicesNullClientV1';
import { DevicesDirectClientV1 } from '../version1/DevicesDirectClientV1';
import { DevicesHttpClientV1 } from '../version1/DevicesHttpClientV1';


export class DevicesClientFactory extends Factory {
	public static Descriptor: Descriptor = new Descriptor('ad-board-devices', 'factory', 'default', 'default', '1.0');
	public static NullClientV1Descriptor = new Descriptor('ad-board-devices', 'client', 'null', 'default', '1.0');
	public static DirectClientV1Descriptor = new Descriptor('ad-board-devices', 'client', 'direct', 'default', '1.0');
	public static HttpClientV1Descriptor = new Descriptor('ad-board-devices', 'client', 'http', 'default', '1.0');

	
	constructor() {
		super();

		this.registerAsType(DevicesClientFactory.NullClientV1Descriptor, DevicesNullClientV1);
		this.registerAsType(DevicesClientFactory.DirectClientV1Descriptor, DevicesDirectClientV1);
		this.registerAsType(DevicesClientFactory.HttpClientV1Descriptor, DevicesHttpClientV1);
	}
	
}
