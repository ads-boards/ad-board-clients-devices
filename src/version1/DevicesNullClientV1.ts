import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams} from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';

import { IDevicesClientV1 } from './IDevicesClientV1';
import { DeviceV1 } from './DeviceV1';

export class DevicesNullClientV1 implements IDevicesClientV1 {
            
    public getDevices(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<DeviceV1>) => void): void {
        callback(null, new DataPage<DeviceV1>([], 0));
    }

    public getDeviceById(correlationId: string, deviceId: string, 
        callback: (err: any, item: DeviceV1) => void): void {
        callback(null, null);
    }

    public createDevice(correlationId: string, device: DeviceV1, 
        callback: (err: any, item: DeviceV1) => void): void {
        callback(null, device);
    }

    public updateDevice(correlationId: string, device: DeviceV1, 
        callback: (err: any, item: DeviceV1) => void): void {
        callback(null, device);
    }

    public deleteDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, item: DeviceV1) => void): void {
        callback(null, null);
    }

    generateRequstCode(correlationId: string, deviceId: string,
        callback: (err: any, code: string) => void): void{
            callback(null, null);
        }
}