import { ConfigParams } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { CommandableHttpClient } from 'pip-services3-rpc-node';

import { DeviceV1 } from './DeviceV1';
import { IDevicesClientV1 } from './IDevicesClientV1';

export class DevicesHttpClientV1 extends CommandableHttpClient implements IDevicesClientV1 {

    constructor(config?: any) {
        super('v1/devices');

        if (config != null)
            this.configure(ConfigParams.fromValue(config));
    }

    public getDevices(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DeviceV1>) => void): void {
        this.callCommand(
            'get_devices',
            correlationId,
            {
                filter: filter,
                paging: paging
            },
            callback
        );
    }

    public getDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, item: DeviceV1) => void): void {
        this.callCommand(
            'get_device_by_id',
            correlationId,
            {
                device_id: deviceId
            },
            callback
        );
    }

    public createDevice(correlationId: string, device: DeviceV1,
        callback: (err: any, item: DeviceV1) => void): void {
        this.callCommand(
            'create_device',
            correlationId,
            {
                device: device
            },
            callback
        );
    }

    public updateDevice(correlationId: string, device: DeviceV1,
        callback: (err: any, item: DeviceV1) => void): void {
        this.callCommand(
            'update_device',
            correlationId,
            {
                device: device
            },
            callback
        );
    }

    public deleteDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, item: DeviceV1) => void): void {
        this.callCommand(
            'delete_device_by_id',
            correlationId,
            {
                device_id: deviceId
            },
            callback
        );
    }

    public generateRequstCode(correlationId: string, deviceId: string,
        callback: (err: any, code: string) => void): void {
        this.callCommand(
            'generate_request_code',
            correlationId,
            {
                device_id: deviceId
            }, (err, result) => {
                callback(err, result ? result.code : null);
            }
        );
    }
}
