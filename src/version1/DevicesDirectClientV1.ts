import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { DirectClient } from 'pip-services3-rpc-node';

import { IDevicesClientV1 } from './IDevicesClientV1';

import { DeviceV1 } from './DeviceV1';

export class DevicesDirectClientV1 extends DirectClient<any> implements IDevicesClientV1 {

    public constructor() {
        super();
        this._dependencyResolver.put('controller', new Descriptor("ad-board-devices", "controller", "*", "*", "*"))
    }

    public getDevices(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DeviceV1>) => void): void {
        let timing = this.instrument(correlationId, 'devices.get_devices');
        this._controller.getDevices(correlationId, filter, paging, (err, page) => {
            timing.endTiming();
            callback(err, page);
        });
    }

    public getDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, item: DeviceV1) => void): void {
        let timing = this.instrument(correlationId, 'devices.get_device_by_id');
        this._controller.getDeviceById(correlationId, deviceId, (err, device) => {
            timing.endTiming();
            callback(err, device);
        });
    }

    public createDevice(correlationId: string, device: DeviceV1,
        callback: (err: any, item: DeviceV1) => void): void {
        let timing = this.instrument(correlationId, 'devices.create_device');
        this._controller.createDevice(correlationId, device, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public updateDevice(correlationId: string, device: DeviceV1,
        callback: (err: any, item: DeviceV1) => void): void {
        let timing = this.instrument(correlationId, 'devices.update_device');
        this._controller.updateDevice(correlationId, device, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    public deleteDeviceById(correlationId: string, deviceId: string,
        callback: (err: any, item: DeviceV1) => void): void {
        let timing = this.instrument(correlationId, 'devices.delete_device_by_id');
        this._controller.deleteDeviceById(correlationId, deviceId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }

    generateRequstCode(correlationId: string, deviceId: string,
        callback: (err: any, code: string) => void): void {
        let timing = this.instrument(correlationId, 'devices.generate_request_code');
        this._controller.generateRequstCode(correlationId, deviceId, (err, code) => {
            timing.endTiming();
            callback(err, code);
        });
    }
}