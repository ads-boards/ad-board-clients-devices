"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesHttpClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicesHttpClientV1 extends pip_services3_rpc_node_1.CommandableHttpClient {
    constructor(config) {
        super('v1/devices');
        if (config != null)
            this.configure(pip_services3_commons_node_1.ConfigParams.fromValue(config));
    }
    getDevices(correlationId, filter, paging, callback) {
        this.callCommand('get_devices', correlationId, {
            filter: filter,
            paging: paging
        }, callback);
    }
    getDeviceById(correlationId, deviceId, callback) {
        this.callCommand('get_device_by_id', correlationId, {
            device_id: deviceId
        }, callback);
    }
    createDevice(correlationId, device, callback) {
        this.callCommand('create_device', correlationId, {
            device: device
        }, callback);
    }
    updateDevice(correlationId, device, callback) {
        this.callCommand('update_device', correlationId, {
            device: device
        }, callback);
    }
    deleteDeviceById(correlationId, deviceId, callback) {
        this.callCommand('delete_device_by_id', correlationId, {
            device_id: deviceId
        }, callback);
    }
    generateRequstCode(correlationId, deviceId, callback) {
        this.callCommand('generate_request_code', correlationId, {
            device_id: deviceId
        }, (err, result) => {
            callback(err, result ? result.code : null);
        });
    }
}
exports.DevicesHttpClientV1 = DevicesHttpClientV1;
//# sourceMappingURL=DevicesHttpClientV1.js.map