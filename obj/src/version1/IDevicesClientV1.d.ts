import { DataPage } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DeviceV1 } from './DeviceV1';
export interface IDevicesClientV1 {
    getDevices(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DeviceV1>) => void): void;
    getDeviceById(correlationId: string, deviceId: string, callback: (err: any, item: DeviceV1) => void): void;
    createDevice(correlationId: string, device: DeviceV1, callback: (err: any, item: DeviceV1) => void): void;
    updateDevice(correlationId: string, device: DeviceV1, callback: (err: any, item: DeviceV1) => void): void;
    deleteDeviceById(correlationId: string, deviceId: string, callback: (err: any, item: DeviceV1) => void): void;
    generateRequstCode(correlationId: string, deviceId: string, callback: (err: any, code: string) => void): void;
}
