"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesNullClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
class DevicesNullClientV1 {
    getDevices(correlationId, filter, paging, callback) {
        callback(null, new pip_services3_commons_node_1.DataPage([], 0));
    }
    getDeviceById(correlationId, deviceId, callback) {
        callback(null, null);
    }
    createDevice(correlationId, device, callback) {
        callback(null, device);
    }
    updateDevice(correlationId, device, callback) {
        callback(null, device);
    }
    deleteDeviceById(correlationId, deviceId, callback) {
        callback(null, null);
    }
    generateRequstCode(correlationId, deviceId, callback) {
        callback(null, null);
    }
}
exports.DevicesNullClientV1 = DevicesNullClientV1;
//# sourceMappingURL=DevicesNullClientV1.js.map