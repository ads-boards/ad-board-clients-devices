"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesDirectClientV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicesDirectClientV1 extends pip_services3_rpc_node_1.DirectClient {
    constructor() {
        super();
        this._dependencyResolver.put('controller', new pip_services3_commons_node_1.Descriptor("ad-board-devices", "controller", "*", "*", "*"));
    }
    getDevices(correlationId, filter, paging, callback) {
        let timing = this.instrument(correlationId, 'devices.get_devices');
        this._controller.getDevices(correlationId, filter, paging, (err, page) => {
            timing.endTiming();
            callback(err, page);
        });
    }
    getDeviceById(correlationId, deviceId, callback) {
        let timing = this.instrument(correlationId, 'devices.get_device_by_id');
        this._controller.getDeviceById(correlationId, deviceId, (err, device) => {
            timing.endTiming();
            callback(err, device);
        });
    }
    createDevice(correlationId, device, callback) {
        let timing = this.instrument(correlationId, 'devices.create_device');
        this._controller.createDevice(correlationId, device, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    updateDevice(correlationId, device, callback) {
        let timing = this.instrument(correlationId, 'devices.update_device');
        this._controller.updateDevice(correlationId, device, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    deleteDeviceById(correlationId, deviceId, callback) {
        let timing = this.instrument(correlationId, 'devices.delete_device_by_id');
        this._controller.deleteDeviceById(correlationId, deviceId, (err, item) => {
            timing.endTiming();
            callback(err, item);
        });
    }
    generateRequstCode(correlationId, deviceId, callback) {
        let timing = this.instrument(correlationId, 'devices.generate_request_code');
        this._controller.generateRequstCode(correlationId, deviceId, (err, code) => {
            timing.endTiming();
            callback(err, code);
        });
    }
}
exports.DevicesDirectClientV1 = DevicesDirectClientV1;
//# sourceMappingURL=DevicesDirectClientV1.js.map