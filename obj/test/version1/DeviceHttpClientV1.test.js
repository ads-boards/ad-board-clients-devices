"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let assert = require('chai').assert;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const ad_board_services_Devices_node_1 = require("ad-board-services-Devices-node");
const ad_board_services_Devices_node_2 = require("ad-board-services-Devices-node");
const ad_board_services_Devices_node_3 = require("ad-board-services-Devices-node");
const DevicesHttpClientV1_1 = require("../../src/version1/DevicesHttpClientV1");
const DevicesClientFixtureV1_1 = require("./DevicesClientFixtureV1");
var httpConfig = pip_services3_commons_node_2.ConfigParams.fromTuples("connection.protocol", "http", "connection.host", "localhost", "connection.port", 3000);
suite('DevicesRestClientV1', () => {
    let service;
    let client;
    let fixture;
    suiteSetup((done) => {
        let logger = new pip_services3_components_node_1.ConsoleLogger();
        let persistence = new ad_board_services_Devices_node_1.DevicesMemoryPersistence();
        let controller = new ad_board_services_Devices_node_2.DevicesController();
        service = new ad_board_services_Devices_node_3.DevicesHttpServiceV1();
        service.configure(httpConfig);
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_1.Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger, new pip_services3_commons_node_1.Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_1.Descriptor('ad-board-devices', 'controller', 'default', 'default', '1.0'), controller, new pip_services3_commons_node_1.Descriptor('ad-board-devices', 'service', 'http', 'default', '1.0'), service);
        controller.setReferences(references);
        service.setReferences(references);
        client = new DevicesHttpClientV1_1.DevicesHttpClientV1();
        client.setReferences(references);
        client.configure(httpConfig);
        fixture = new DevicesClientFixtureV1_1.DevicesClientFixtureV1(client);
        service.open(null, (err) => {
            client.open(null, done);
        });
    });
    suiteTeardown((done) => {
        client.close(null);
        service.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
});
//# sourceMappingURL=DeviceHttpClientV1.test.js.map