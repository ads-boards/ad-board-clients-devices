"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let assert = require('chai').assert;
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const ad_board_services_Devices_node_1 = require("ad-board-services-Devices-node");
const ad_board_services_Devices_node_2 = require("ad-board-services-Devices-node");
const DevicesDirectClientV1_1 = require("../../src/version1/DevicesDirectClientV1");
const DevicesClientFixtureV1_1 = require("./DevicesClientFixtureV1");
suite('DevicesDirectClientV1', () => {
    let client;
    let fixture;
    suiteSetup((done) => {
        let logger = new pip_services3_components_node_1.ConsoleLogger();
        let persistence = new ad_board_services_Devices_node_1.DevicesMemoryPersistence();
        let controller = new ad_board_services_Devices_node_2.DevicesController();
        let references = pip_services3_commons_node_2.References.fromTuples(new pip_services3_commons_node_1.Descriptor('pip-services', 'logger', 'console', 'default', '1.0'), logger, new pip_services3_commons_node_1.Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_1.Descriptor('ad-board-devices', 'controller', 'default', 'default', '1.0'), controller);
        controller.setReferences(references);
        client = new DevicesDirectClientV1_1.DevicesDirectClientV1();
        client.setReferences(references);
        fixture = new DevicesClientFixtureV1_1.DevicesClientFixtureV1(client);
        client.open(null, done);
    });
    suiteTeardown((done) => {
        client.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
});
//# sourceMappingURL=DeviceDirectClientV1.test.js.map