import { IDevicesClientV1 } from '../../src/version1/IDevicesClientV1';
export declare class DevicesClientFixtureV1 {
    private _client;
    constructor(client: IDevicesClientV1);
    testCrudOperations(done: any): void;
}
